###################
QUEUE Project
###################

Projecto en CodeIgniter 3 y PHP 7.1.

Available Endpoints:

-  localhost/queue/api/jobs (GET): lista el trabajo más antiguo (más prioridad) en estado 0 (pendiente)
-  localhost/queue/api/jobs (POST): crear un trabajo nuevo - envio params: submitter_id (int), processor_id (int), command (string), por defecto campo: status en 0
-  localhost/queue/api/jobs/[99] (GET): lista un trabajo en especifico
-  localhost/queue/api/jobs/[99] (PUT): actualiza un trabajo en especifico: el campo status = 1, para procesar un trabajo. El campo status = 2, para marcar como completado un trabajo.
-  localhost/queue/api/jobs/[99] (DELETE): borra un trabajo en especifico
-  localhost/queue/api/jobs/average/ (POST): obtiene métrica de tiempo promedio en que se completan los trabajos, en horas y minutos.

*******************
Base de Datos
*******************

Adjunto se encuentra el archivo SQL con la estructura de la BD.

-  Usuario: root
-  Clave: 