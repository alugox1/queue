<?php
   
   require APPPATH . '/libraries/REST_Controller.php';
   use Restserver\Libraries\REST_Controller;
     
class Job extends REST_Controller {
    
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("jobs", ['id' => $id])->row_array();
        } else {
            $data = $this->db->get_where("jobs", ["status" => "0"])->row_array();
        }

        $this->response($data, REST_Controller::HTTP_OK);
	}

    public function index_post()
    {
        $input = $this->input->post();
        if (isset($input["processor_id"])) {
            if ($input["processor_id"] != "" && $input["processor_id"] != 0) {
                $input["status"] = 1; // job is being processed
            }
        }
        
        $this->db->insert('jobs',$input);

        $insert_id = $this->db->insert_id();
     
        $this->response(['Job #'.$insert_id.' created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    public function index_put($id)
    {
        $input = $this->put();
        if (isset($input["status"])) {
            if ($input["status"] == "2") {
                $input["date_completed"] = date("Y-m-d H:i:s");
            }
        }

        $this->db->update('jobs', $input, array('id'=>$id));
     
        $this->response(['Job status updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    public function index_delete($id)
    {
        $this->db->delete('jobs', array('id'=>$id));
       
        $this->response(['Job deleted successfully.'], REST_Controller::HTTP_OK);
    }

    public function average_post()
    {
        $query = $this->db->select('DATE(date_created) AS startdate, AVG(TIME_TO_SEC(TIMEDIFF(date_completed,date_created))) AS timediff')->get('jobs')->row_array();
        $minutes = number_format($query["timediff"] / 60, 2, '.', '');
        $hours = number_format($minutes / 24, 2, '.', '');
        $this->response(['Average Processing Time: '.$minutes.' minutes, or '.$hours.' hours.'], REST_Controller::HTTP_OK);

    }

    	
}